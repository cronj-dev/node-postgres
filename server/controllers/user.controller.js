import User from '../models/user.model';


export class userCtrl {
  constructor() {}
  /**
   * Load user and append to req.
   */

   async load(req, res, next, id) {
     try {
       const user = await User.get(id);
       req.user = user;
       return next();
     }
     catch(e) {
       next(e);
     }
   }


  /**
   * Get user
   * @returns {User}
   */
  get(req, res) {
    return res.json(req.user);
  }

  /**
   * Create new user
   * @property {string} req.body.username - The username of user.
   * @property {string} req.body.mobileNumber - The mobileNumber of user.
   * @property {string} req.body.firstName - The firstName of user.
   * @property {string} req.body.lastName - The lastName of user.
   * @returns {User}
   */
  async create(req, res, next) {
    const user = new User({
      username: req.body.username,
      mobileNumber: req.body.mobileNumber,
      firstName: req.body.firstName,
      lastName: req.body.lastName
    });

    try {
      const user = await user.save();
      res.json(user);
    }
    catch(e){
      next(e);
    }
  }

  /**
   * Update existing user
   * @property {string} req.body.username - The username of user.
   * @property {string} req.body.mobileNumber - The mobileNumber of user.
   * @property {string} req.body.firstName - The firstName of user.
   * @property {string} req.body.lastName - The lastName of user.
   */
  async update(req, res, next) {
    const user = req.user;
    user.username = req.body.username;
    user.mobileNumber = req.body.mobileNumber;
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;

    try {
      const user = await user.save();
      res.json(user);
    }
    catch(e){
      next(e);
    }
  }

  /**
   * Get user list.
   * @property {number} req.query.skip - Number of users to be skipped.
   * @property {number} req.query.limit - Limit number of users to be returned.
   * @returns {User[]}
   */
  async list(req, res, next) {
    const { limit = 50, skip = 0 } = req.query;
    try {
      const users = await User.list({ limit, skip });
      res.json(users);
    }
    catch(e){
      next(e);
    }
  }

  /**
   * Delete user.
   * @returns {User}
   */
  async remove(req, res, next) {
    const user = req.user;
    try {
        const deletedUser = await user.remove();
    }
    catch(e){
      next(e);
    }
 }
}


export default new userCtrl();
