import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import methodOverride from 'method-override';
import cors from 'cors';
import httpStatus from 'http-status';
import expressWinston from 'express-winston';
import expressValidation from 'express-validation';
import helmet from 'helmet';
import path from 'path';
import appRoot from 'app-root-path';
import winston from 'winston';
import config from './config';
import APIError from '../helpers/APIError';
import routes from '../routes/index.routes';

const app = express();


if (config.env === 'development') {
  app.use(logger('dev'));
}

/* parse body params and attach it into req.body */

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* parse cooke header and  get the cookies using req.cookies */
app.use(cookieParser());

/* to compress all http response */
app.use(compression());

/*override the all http verbs or methods */
app.use(methodOverride());

/* secure http headers */
app.use(helmet());

/* enable cross origin resource*/
app.use(cors());


/* enable detailed all API logging in dev env */
if (config.env === 'development') {
  app.use(expressWinston.logger({
      transports: [
        new winston.transports.Console({
          json: true,
          colorize: true
        })
      ],
      meta: true, // optional: control whether you want to log the meta data about the request (default to true)
      msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
      expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
      colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
      ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
    }));
}

app.use('/api', routes);

/*Validate the body, params, headers and query if validation fails return a response with error.*/
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

/* Middleware to catch api which is not found and pass it into next error handler */
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});

// // error handler, send stacktrace only during development
// app.use((err, req, res, next) => // eslint-disable-line no-unused-vars
//   res.status(err.status).json({
//     message: err.isPublic ? err.message : httpStatus[err.status],
//     stack: config.env === 'development' ? err.stack : {}
//   })
// );
//

export default app;
