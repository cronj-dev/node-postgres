# Node-Postgres-Express-Mocha Boilerplate

This is node-postgres-express-mocha starterkit.

## Getting Started

Clone the project and then create the .env file in root directory which will be used for create environment and set the configuration.

EXAMPLE :

***ENVIRONMENT = development***

***PORT = {Whatever port you want to use}***

***PG_HOST = {PostgresDB URL}***

***PORT =  {PORT number in which Postgres is running}***

## Prerequisite Technologies

 ***Node 7 + ES6 (Babel)***

 ***Express***

 ***Postgres***

 ***Joi***

 ***Mocha***


### Installation


Install [NodeJS](https://nodejs.org/en/download/)


git clone  https://karnprem90@bitbucket.org/cronj-dev/node-postgres.git

***cd NODE-Postgres***

***npm install***

***npm start***




## Running the tests

***npm test***




## Contributing



## Versioning
***1.0.0***

## Authors

***CronJ IT Technologies Pvt. Ltd.***

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
