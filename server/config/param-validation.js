import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required(),
      firstName: Joi.string(),
      lastName: Joi.string()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      username: Joi.string().required(),
      mobileNumber: Joi.string().regex(/^[1-9][0-9]{9}$/).required(),
      firstName: Joi.string(),
      lastName: Joi.string()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  }
};
